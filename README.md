# lazyouts

A simple CSS Grid framework for the rest of us.

Currently v1.1.0

---

## Installation

`npm install lazyouts`

## About

lazyouts is an unopinionated CSS grid layout system. It's lightweight at __ and contains only the basics of what you need to build complex CSS layouts for desktop, tablet and mobile. 

You can and should DIY by writing your own CSS grid layouts for smaller sites, afterall it's pretty straightforward once you get the hang of it. Here is a [great tutorial](https://css-tricks.com/snippets/css/complete-guide-grid/) on how to use CSS grid if you're unfamiliar with it. 

But if you're lazy like me and would rather not write the same grid layouts over and over again well then my friend simply `npm install lazyouts` and write 1-4 class names on a `<div>` like this `<div class="layout d4c t2c m1c"></div>` for instance to build a simple layout that works across all devices. In this example the layout would be 4 columns on desktop, 2 columns on tablet and 1 column in mobile. The columns will be created by nesting any number of child `div` tags inside of `<div class="layout d4c t2c m1c"></div>`. Any excess `div` children above the specified amount in your layout will simply wrap to a new row. Learn more about all of the types of layouts you can create and just how lazy you can get with class naming in the usage section.

## Usage

I'm too lazy to write this write now. Merr.

## License

  
The MIT License (MIT)

Copyright (c) 2019 QuotedCar
Copyright (c) 2019 The lazyout Authors

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Attribution

lazouts was created at QuotedCar by Tyler Richardson and Jason McBurney